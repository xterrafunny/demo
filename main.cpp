#include <iostream>

long long Pow(long long base, long long exp) {
    long long return_val = 0;
    if (exp == 0) {
        return_val = 1;
    } else if (exp % 2 == 0) {
        long long half_exp_pow = Pow(base, exp / 2);
        return_val = half_exp_pow * half_exp_pow;
    } else {
        return_val = base * Pow(base, exp - 1);
    }
    return return_val;
}

int main() {
    long long a, b;
    std::cin >> a >> b;
    std::cout << Pow(a, b) << std::endl;
}
